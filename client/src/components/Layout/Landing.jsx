import React from 'react';
import { Navigate } from 'react-router';

function Landing(props) {
    return (
        <Navigate to="/login" replace/>
    );
}

export default Landing;