import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link } from 'react-router-dom';

const Login = () => {
    return (
        <>
            <Form className="my-4">
                <Form.Group>
                    <Form.Control type="text" placeholder="Username" name="username" required />
                </Form.Group>
                <Form.Group>
                    <Form.Control type="text" placeholder="Password" name="password" required />
                </Form.Group>
                <Button type="submit" variant="success">
                    Login
                </Button>
            </Form>
            <p>
                Dont have an account
                <Link to="/register">
                    <Button variant="info" size="sm" className="m1-2">
                        Register
                    </Button>
                </Link>
            </p>
        </>
    );
};

export default Login;
