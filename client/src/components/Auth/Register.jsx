import React from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link } from 'react-router-dom';

const Register = () => {
    return (
        <>
            <Form className="my-4">
                <Form.Group>
                    <Form.Control type="text" placeholder="Username" name="username" required />
                </Form.Group>
                <Form.Group>
                    <Form.Control type="text" placeholder="Password" name="password" required />
                </Form.Group>
                <Form.Group>
                    <Form.Control type="text" placeholder="Cofirm password" name="confirmPassword" required />
                </Form.Group>
                <Button type="submit" variant="success">
                    Register
                </Button>
            </Form>
            <p>
                I have an account
                <Link to="/login">
                    <Button variant="info" size="sm" className="m1-2">
                        Login
                    </Button>
                </Link>
            </p>
        </>
    );
};

export default Register;
