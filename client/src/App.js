import './App.css';
import { useRoutes } from 'react-router';
import routes from './routes';
import React, { Suspense } from 'react';
import Spinner from 'react-bootstrap/Spinner';

function App() {
    const routing = useRoutes(routes);

    return (
        <Suspense
            fallback={
                <div style={{ height: '100%' }}>
                    <Spinner animation="border" variant="primary" />
                </div>
            }
        >
            {routing}
        </Suspense>
    );
}

export default App;
