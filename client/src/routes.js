import React, { lazy } from 'react';
import { Navigate } from 'react-router';

const Landing = lazy(() => import('./components/Layout/Landing'));
const Login = lazy(() => import('./components/Auth/Login'));
const Register = lazy(() => import('./components/Auth/Register'));
const NotFound = lazy(() => import('./components/Layout/NotFound'));

const routes = [
    {
        path: '/',
        element: <Landing />,
        children: [{ path: '*', element: <Navigate to="/not-found" replace /> }],
    },
    {
        path: '/login',
        element: <Login />,
    },
    {
        path: '/register',
        element: <Register />,
    },
    {
        path: 'not-found',
        element: <NotFound />,
    },
];

export default routes;
