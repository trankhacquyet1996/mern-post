const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const verifyToken = require('../middleware/auth');
const User = require('../models/Users');

// @route GET api/auth
// @desc Check if user is logged in
// @access Public
router.get('/', verifyToken, async (req, res) => {
    try {
        const user = await User.findById(req.userId).select('-password');
        if (!user) return res.status(400).json({ success: false, message: 'User not found' });
        res.json({ success: true, user });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: 'Internal server error' });
    }
});

// @route post api/auth/register
// @decs register user
// @access public

router.post('/register', async (req, res) => {
    const { userName, password } = req.body;
    // simple validation
    if (!userName || !password) {
        return res.status(400).json({ success: false, message: 'Missing user name or password' });
    }
    try {
        // check if user existing
        const user = await User.findOne({ userName });
        if (user) return res.status(400).json({ success: true, message: 'User already exists' });
        const hashPassword = await argon2.hash(password);
        const newUser = new User({
            userName,
            password: hashPassword,
        });
        await newUser.save();
        //return tokens
        const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN_SECRET);
        res.json({ success: true, message: 'create successfully', accessToken });
    } catch (e) {
        console.log(e);
        res.status(500).json({ success: false, message: 'Internal Error' });
    }
});

// @route post api/auth/login
// @decs Login user
// @access public

router.post('/login', async (req, res) => {
    const { userName, password } = req.body;
    if (!userName || !password) {
        return res.status(400).json({ success: false, message: 'Missing user name or password' });
    }
    try {
        const user = await User.findOne({ userName });
        if (!user) {
            return res.status(400).json({ success: false, message: 'Incorrect user name or password' });
        }
        // validation password
        const passwordValidate = await argon2.verify(user.password, password);
        if (!passwordValidate) {
            return res.status(400).json({ success: false, message: 'Incorrect user name or password' });
        }
        // all good
        // return token
        const accessToken = jwt.sign({ userId: user._id }, process.env.ACCESS_TOKEN_SECRET);
        res.json({ success: true, message: 'Login successfully', accessToken });
    } catch (error) {
        console.log(e);
        res.status(500).json({ success: false, message: 'Internal Error' });
    }
});

module.exports = router;
