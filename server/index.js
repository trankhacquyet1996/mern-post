require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');

const authRouter = require('./routes/Auth');
const postRouter = require('./routes/Posts');

const connectDB = async () => {
    try {
        await mongoose.connect(
            `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@mern-post.wwjdeaa.mongodb.net/?retryWrites=true&w=majority`
        );
        console.log('MongoDB Connected......');
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

connectDB();

const app = express();
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/posts', postRouter);

const port = 5000;
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
